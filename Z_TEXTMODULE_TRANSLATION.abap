*&---------------------------------------------------------------------*
*& Report Z_TEXTMODULE_TRANSLATION
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_textmodule_translation.
PARAMETERS: p_file  TYPE rlgrap-filename OBLIGATORY. "To enter file name.
PARAMETERS: p_task  TYPE e070-trkorr." OBLIGATORY."To enter TR number.
*CALL FUNCTION 'F4_FILENAME'
*      IMPORTING
*        file_name = p_file.

INITIALIZATION.

CLASS cl_text_translation DEFINITION.

  PUBLIC SECTION.

    DATA: i_file  TYPE rlgrap-filename .

    TYPES: BEGIN OF ty_str,
             targetlang TYPE tlanguage,
             sourcelang TYPE tlanguage,
             masterlang TYPE tlanguage,
             text_id    TYPE tdobname,
             text       TYPE tdline,
             objname    TYPE objname,
             devclass   TYPE devclass,
           END OF ty_str.

    DATA: lt_type  TYPE truxs_t_text_data,
          lt_excel TYPE TABLE OF ty_str,
          ls_excel TYPE ty_str.

    DATA: lt_excel_temp TYPE STANDARD TABLE OF ztask,
          ls_excel_temp LIKE LINE OF lt_excel_temp.

    DATA: lt_targettext  TYPE TABLE OF tline,
          lt_textupdated TYPE TABLE OF tline,
          lt_textupdated_error TYPE TABLE OF tline,
          ls_targettext  TYPE tline,
          ls_lwrkobj     TYPE lwrkobj.


    METHODS:constructor IMPORTING p_file  TYPE rlgrap-filename ,
            f4_filename," IMPORTING i_file TYPE rlgrap-filename,
            upload_excel,
            upload_to_db,
            update_tr,
            table_display,
            tables_display.
ENDCLASS.

CLASS cl_text_translation IMPLEMENTATION.

  METHOD constructor.

    i_file = p_file.

  ENDMETHOD.

  METHOD f4_filename.

    CALL FUNCTION 'F4_FILENAME'
      IMPORTING
        file_name = i_file.

  ENDMETHOD.

  METHOD upload_excel.

    CALL FUNCTION 'TEXT_CONVERT_XLS_TO_SAP'
      EXPORTING
        i_tab_raw_data       = lt_type
        i_filename           = i_file  "Filename
      TABLES
        i_tab_converted_data = lt_excel
      EXCEPTIONS
        conversion_failed    = 1
        OTHERS               = 2.

    IF sy-subrc NE  0.
      MESSAGE ID sy-msgid
      TYPE sy-msgty
      NUMBER sy-msgno
      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ENDMETHOD.
  METHOD upload_to_db.

  DELETE lt_excel INDEX 1. " delete header of excel sheet

  MOVE-CORRESPONDING lt_excel TO lt_excel_temp.

****** Update database table
*  IF lt_excel_temp IS NOT INITIAL.

*MODIFY ztask FROM TABLE lt_excel_temp.
  LOOP AT lt_excel ASSIGNING FIELD-SYMBOL(<lf_excel>).

    ls_targettext-tdformat = '/E'.
    ls_targettext-tdline   = '&%TEXT1&'.
    APPEND ls_targettext TO lt_targettext.
    CLEAR:ls_targettext.
    ls_targettext-tdline   = <lf_excel>-text.
    APPEND ls_targettext TO lt_targettext.
    CLEAR:ls_targettext.

    ls_lwrkobj-targetlang = <lf_excel>-targetlang.
    ls_lwrkobj-masterlang = <lf_excel>-masterlang.
    ls_lwrkobj-sourcelang = <lf_excel>-sourcelang.
    ls_lwrkobj-objtype    = 'SSF'.
    ls_lwrkobj-objname    = <lf_excel>-objname.
    ls_lwrkobj-devclass   = <lf_excel>-devclass.
    ls_lwrkobj-status = 'M'.
    ls_lwrkobj-linestotal = 2.
    ls_lwrkobj-linesmodif = 2.
    ls_lwrkobj-uname      = sy-uname.
    ls_lwrkobj-udate      = sy-datum.
    ls_lwrkobj-utime      = sy-uzeit.


    CALL FUNCTION 'SSFTR_SET_TEXT'
      EXPORTING
        targetlang      = <lf_excel>-targetlang
        sourcelang      = <lf_excel>-sourcelang
        docstate        = 'R'
      TABLES
        targettext      = lt_targettext
      CHANGING
        tlwrkobj        = ls_lwrkobj
      EXCEPTIONS
        error_in_update = 1
        OTHERS          = 2.
    CLEAR: ls_lwrkobj.

    IF sy-subrc = 0.
      APPEND <lf_excel> TO lt_textupdated.
    ELSE.
      APPEND <lf_excel> TO lt_textupdated_error.
    ENDIF.

  ENDLOOP.



  IF sy-subrc EQ 0.

    IF lt_textupdated IS NOT INITIAL.

    ENDIF.

  ENDIF.

  ENDMETHOD.

METHOD update_tr.

  DATA: gw_e071 TYPE e071.
  DATA: gt_e071 TYPE TABLE OF e071.
  DATA: lt_e071k TYPE TABLE OF e071k.
    IF NOT lt_excel IS INITIAL.
    SELECT spras,
    txtype,
    formname,
    iname,
    vari,
    linenr,
    tdformat,
    tdline
    FROM stxftxt INTO TABLE @DATA(gt_texts)
    FOR ALL ENTRIES IN @lt_excel
    WHERE formname = @lt_excel-objname AND
    spras    = @lt_excel-masterlang.

    IF sy-subrc IS INITIAL.
      SORT gt_texts BY formname ASCENDING.
      DELETE gt_texts WHERE tdline IS INITIAL.
      DELETE ADJACENT DUPLICATES FROM gt_texts COMPARING formname spras.


*        SELECT SINGLE MAX( as4pos ) INTO lv_as4pos FROM e071 WHERE trkorr = task.
      SELECT * FROM e071 INTO TABLE @DATA(lt_e071_tmp)
         WHERE trkorr = @p_task.
      IF sy-subrc IS INITIAL.
        SORT lt_e071_tmp BY as4pos DESCENDING.
        READ TABLE lt_e071_tmp INTO DATA(lw_e071_tmp) INDEX 1.
        IF  sy-subrc IS INITIAL.
          DATA(lv_as4pos) = lw_e071_tmp-as4pos.
          CLEAR lw_e071_tmp.
        ENDIF.
        DELETE lt_e071_tmp WHERE object <> 'SSFO' AND pgmid <> 'LANG'.
      ENDIF.

      LOOP AT gt_texts ASSIGNING FIELD-SYMBOL(<lf_texts>).
        gw_e071-trkorr  = p_task.
        gw_e071-as4pos  = lv_as4pos + 1.
        gw_e071-pgmid   = 'LANG'.
        gw_e071-object  = 'SSFO'.

        gw_e071-obj_name = <lf_texts>-formname.
*          gw_e071-lang    = sy-langu.
        LOOP AT lt_excel ASSIGNING FIELD-SYMBOL(<lf_exc>).
          IF line_exists( lt_excel[ objname = <lf_texts>-formname ]  ).
            gw_e071-lang = lt_excel[ objname = <lf_texts>-formname  ]-targetlang.
            CLEAR: <lf_exc>.
            APPEND gw_e071 TO gt_e071.
          ENDIF.
        ENDLOOP.

        CLEAR gw_e071.
      ENDLOOP.
    ENDIF.
  ENDIF.

  IF NOT gt_e071 IS INITIAL.

    CALL FUNCTION 'TR_APPEND_TO_COMM_OBJS_KEYS'
      EXPORTING
        wi_trkorr                      = p_task
      TABLES
        wt_e071                        = gt_e071
        wt_e071k                       = lt_e071k
      EXCEPTIONS
        key_char_in_non_char_field     = 1
        key_check_keysyntax_error      = 2
        key_inttab_table               = 3
        key_longer_field_but_no_generc = 4
        key_missing_key_master_fields  = 5
        key_missing_key_tablekey       = 6
        key_non_char_but_no_generic    = 7
        key_no_key_fields              = 8
        key_string_longer_char_key     = 9
        key_table_has_no_fields        = 10
        key_table_not_activ            = 11
        key_unallowed_key_function     = 12
        key_unallowed_key_object       = 13
        key_unallowed_key_objname      = 14
        key_unallowed_key_pgmid        = 15
        key_without_header             = 16
        ob_check_obj_error             = 17
        ob_devclass_no_exist           = 18
        ob_empty_key                   = 19
        ob_generic_objectname          = 20
        ob_ill_delivery_transport      = 21
        ob_ill_lock                    = 22
        ob_ill_parts_transport         = 23
        ob_ill_source_system           = 24
        ob_ill_system_object           = 25
        ob_ill_target                  = 26
        ob_inttab_table                = 27
        ob_local_object                = 28
        ob_locked_by_other             = 29
        ob_modif_only_in_modif_order   = 30
        ob_name_too_long               = 31
        ob_no_append_of_corr_entry     = 32
        ob_no_append_of_c_member       = 33
        ob_no_consolidation_transport  = 34
        ob_no_original                 = 35
        ob_no_shared_repairs           = 36
        ob_no_systemname               = 37
        ob_no_systemtype               = 38
        ob_no_tadir                    = 39
        ob_no_tadir_not_lockable       = 40
        ob_privat_object               = 41
        ob_repair_only_in_repair_order = 42
        ob_reserved_name               = 43
        ob_syntax_error                = 44
        ob_table_has_no_fields         = 45
        ob_table_not_activ             = 46
        tr_enqueue_failed              = 47
        tr_errors_in_error_table       = 48
        tr_ill_korrnum                 = 49
        tr_lockmod_failed              = 50
        tr_lock_enqueue_failed         = 51
        tr_not_owner                   = 52
        tr_no_systemname               = 53
        tr_no_systemtype               = 54
        tr_order_not_exist             = 55
        tr_order_released              = 56
        tr_order_update_error          = 57
        tr_wrong_order_type            = 58
        ob_invalid_target_system       = 59
        tr_no_authorization            = 60
        ob_wrong_tabletyp              = 61
        ob_wrong_category              = 62
        ob_system_error                = 63
        ob_unlocal_objekt_in_local_ord = 64
        tr_wrong_client                = 65
        ob_wrong_client                = 66
        key_wrong_client               = 67
        OTHERS                         = 68.

    IF sy-subrc <> 0.
* Implement suitable error handling here
    ELSE.

      MESSAGE 'Smart texts moved successfully' TYPE 'I'.
*      LEAVE TO SCREEN 0.

    ENDIF.
  ELSE.

    MESSAGE 'Smart texts already exist' TYPE 'E'.

  ENDIF.

  COMMIT WORK.

ENDMETHOD.

METHOD table_display.

  DATA: gr_table TYPE REF TO cl_salv_table.

  TRY .
    cl_salv_table=>factory( IMPORTING r_salv_table = gr_table CHANGING t_table = lt_textupdated ).
  CATCH cx_salv_msg.

  ENDTRY.


  gr_table->display( ).

ENDMETHOD.

METHOD tables_display.

*DATA(oref1) = NEW cl_demo_output_stream( ).
*
*DATA: output_stream TYPE xstring.
*
*oref->write_data( lt_textupdated ).
*
*oref->write_data( lt_textupdated_error ).
*
*output_stream = oref->close( ).


endmethod.


ENDCLASS.



AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.

  CALL FUNCTION 'F4_FILENAME'
      IMPORTING
        file_name = p_file.

START-OF-SELECTION.

DATA(oref) = NEW cl_text_translation( p_file ).

oref->upload_excel( ).

oref->upload_to_db( ).

IF p_task IS NOT INITIAL.

  oref->update_tr( ).

ENDIF.

*IF sy-subrc IS INITIAL AND oref->lt_excel_temp IS NOT INITIAL.
*
*  oref->table_display( ).
*
*ENDIF.

IF oref->lt_textupdated_error IS NOT INITIAL.

* DATA(stream) = cl_demo_output_stream=>open( ).
*    stream->write_text( iv_text   = 'XML of CL_DEMO_OUTPUT_STREAM'
*                        iv_format = if_demo_output_formats=>heading
*                        iv_level  = 1 ).
*    stream->write_data( ia_value  =  oref->lt_textupdated
*                        iv_format = if_demo_output_formats=>nonprop ).
*    DATA(xml) = stream->close( ).
*    cl_abap_browser=>show_xml( xml_xstring = xml ).
*
*    stream = cl_demo_output_stream=>open( ).
*    SET HANDLER cl_demo_output_text=>handle_output FOR stream.
*    stream->write_text( iv_text   = 'CL_DEMO_OUTPUT_STREAM for Text' ).
*    stream->write_data( ia_value  = itab ).
*    stream->close( ).

cl_demo_output=>begin_section( 'Static Method of CL_DEMO_OUTPUT for HTML' ).
    cl_demo_output=>display( oref->lt_textupdated_error ).
    cl_demo_output=>display( oref->lt_textupdated ).

*DATA: oref1 TYPE REF TO cl_demo_output_stream.
**CREATE OBJECT oref1.
*
*DATA: output_stream TYPE xstring.
*
*oref1->write_data().
*
*oref1->write_data( oref->lt_textupdated_error ).
*
*output_stream = oref1->close( ).


ENDIF.

END-OF-SELECTION. 
